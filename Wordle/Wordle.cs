﻿/*
 AUTHOR: Adrian Rodriguez Bolin
 DATE: 11/12/2022
 DESCRIPTION: Juego Wordle
 */


using System;

namespace Wordle
{
    internal class Wordle
    {
        static void Main(string[] args)
        {
            //INICIAR JUEGO
            bool seguirJugando = true;
            do
            {
                string saberJugar;
                do
                {
                    Console.Clear();
                    Console.WriteLine("Bienvenido juegador. Este es el famoso juego llamado WORDLE.");
                    Console.WriteLine("Sabes como se juega? (Si/No)");
                    saberJugar = Console.ReadLine();
                    if (saberJugar.ToLower() == "no")
                    {
                        Console.WriteLine("\nEl juego consiste en adivinar una palabra escribiendo otras.");
                        Console.WriteLine("Tienes 6 intentos. Las palabras son de 5 letras y ninguna de las letras se repite");
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("Al escribir una palabra si la letra esta en verde quiere decir que esa letra esta en la posicion correcta.");
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Si sale en amarillo quiere decir que esa letra esta en la palabra pero no esta en la posició correcta.");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Y por último, si no sale de ningún color quiere decir que no esta en la palabra.");
                        Console.ReadLine();
                    }
                    else if (saberJugar.ToLower() == "si")
                    {
                        Console.WriteLine("Ok. Pues entonces A JUGAR!!!");
                    }
                    else
                    {
                        Console.WriteLine("No has escrito una respuesta adecuada. Es o un si o un no. Prueba otra vez.");
                    }
                } while (saberJugar.ToLower() != "no" && saberJugar.ToLower() != "si");


                // ESCOGER RANDOM PALABRA

                string[] palabras = new string[20] { "DIOSA", "COJIN", "AVISO", "ABETO", "AJENO", "ABRIL", "HACER", "VARON", "PECAS", "AIREO", "VACIO", "VAGON", "TIGRE", "GUAPO", "APICE", "ALIJO", "AMENO", "EBRIO", "ERIZO", "DEBIL", };
                Random rnd2 = new Random();
                int rnd = rnd2.Next(20);
                string palWordle = palabras[rnd];


                //PRINT MATRIZ VACIA

                char[,] matriz = new char[6, 5];
                char[,] matrizColores = new char[6, 5];
                Console.Clear();
                for (int i = 0; i < 6; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        matriz[i, j] = '_';
                        Console.Write(matriz[i, j] + " ");
                    }
                    Console.WriteLine("");
                }


                int vidas = 6;
                int lineaMatriz = 0;
                do
                {

                    //PEDIR PALABRAS

                    string palUsur;
                    do
                    {
                        Console.WriteLine("Introduce una palabra que contenga 5 letras.");
                        palUsur = Console.ReadLine();
                        if (palUsur.Length != 5)
                        {
                            Console.WriteLine("La palabra que has escrito no tiene 5 caracteres. Vuelve a intentarlo.");
                        }
                    } while (palUsur.Length != 5);
                    Console.Clear();

                    //COMPARAR PALABRA Y RESTAR VIDAS


                    if (palUsur.ToUpper() == palWordle)
                    {
                        Console.Clear();
                        Console.WriteLine("Enhorabuena!!!\nHas adivinado la palabra.\nHas ganado!!!");
                        Console.Write("La palabra era ");
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(palWordle);
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("");
                        break;
                    }
                    else
                    {
                        vidas--;

                        if (vidas == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Vaya, has perdido :(\nTe has quedado sin vidas.");
                            Console.Write("La palabra era ");
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.Write(palWordle);
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("");
                        }
                        else
                        {
                            //PONER PALABRA EN MATRIZ
                            palUsur = palUsur.ToUpper();
                            palWordle = palWordle.ToUpper();

                            char[] charPalUsur = palUsur.ToCharArray();
                            char[] charPalWordle = palWordle.ToCharArray();



                            for (int i = 0; i < 5; i++)
                            {
                                matriz[lineaMatriz, i] = charPalUsur[i];
                            }


                            //COMPARAR LETRA POR LETRA

                            for (int iPalUsur = 0; iPalUsur < 5; iPalUsur++)
                            {
                                for (int iPalWordle = 0; iPalWordle < 5; iPalWordle++)
                                    if (charPalUsur[iPalUsur] == charPalWordle[iPalWordle])
                                    {
                                        matrizColores[lineaMatriz, iPalUsur] = 'y';
                                    }
                            }

                            for (int i = 0; i < 5; i++)
                            {
                                if (charPalUsur[i] == charPalWordle[i])
                                {
                                    matrizColores[lineaMatriz, i] = 'g';
                                }
                            }


                            //PRINT MATRIZ CON COLORES

                            for (int i = 0; i < 6; i++)
                            {
                                for (int j = 0; j < 5; j++)
                                {
                                    if (matrizColores[i, j] == 'g')
                                    {
                                        Console.BackgroundColor = ConsoleColor.Green;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                        Console.Write(matriz[i, j] + " ");
                                        Console.BackgroundColor = ConsoleColor.Black;
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                    else if (matrizColores[i, j] == 'y')
                                    {
                                        Console.BackgroundColor = ConsoleColor.Yellow;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                        Console.Write(matriz[i, j] + " ");
                                        Console.BackgroundColor = ConsoleColor.Black;
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                    else
                                    {
                                        Console.Write(matriz[i, j] + " ");
                                    }
                                }
                                Console.WriteLine("");
                            }


                            //SUBIR VALORES PARA LA SIGUIENTE LINEA

                            lineaMatriz++;
                        }
                    }
                } while (vidas > 0);


                //PREGUNTAR SI QUIERE SEGUIR JUGANDO

                Console.WriteLine("Quieres seguir jugando? (Si/No)");
                string jugarOtraVez = Console.ReadLine();
                if (jugarOtraVez.ToLower() == "no")
                {
                    seguirJugando = false;
                }
            } while (seguirJugando == true);
        }
    }
}
